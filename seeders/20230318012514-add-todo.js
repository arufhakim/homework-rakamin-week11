'use strict'

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Todos', [
      {
        title: 'Mengerjakan Homework Rakamin',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Mengerjakan Exam Rakamin',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Belajar Sequelize CLI',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Belajar CI/CD',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Live Sessions',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Todos', null, {})
  }
}
