const { Todo } = require('../models')

class TodoController {
  static async getTodo (req, res) {
    try {
      const result = await Todo.findAll({
        where: {
          deletedAt: null
        }
      })
      if (result.length === 0) {
        res.status(404).json({ message: 'Todos table is empty!' })
      } else {
        res.status(200).json(result)
      }
    } catch (error) {
      throw new Error(error)
    }
  }

  static async getTodoById (req, res) {
    try {
      const { id } = req.params
      const result = await Todo.findOne({
        where: {
          id,
          deletedAt: null
        }
      })
      if (result === null) {
        res.status(404).json({ message: 'Todos not found!' })
      } else {
        res.status(200).json(result)
      }
    } catch (error) {
      throw new Error(error)
    }
  }

  static async createTodo (req, res) {
    try {
      const { title } = req.body
      const result = await Todo.create({ title })
      res.status(200).json({ message: 'Successfully created todo!', ...result.dataValues })
    } catch (error) {
      throw new Error(error)
    }
  }

  static async deleteTodo (req, res) {
    try {
      const { id } = req.params
      const result = await Todo.destroy({
        where: {
          id
        }
      })
      if (result === 0) {
        res.status(404).json({ message: 'Todos not found!' })
      } else {
        res.status(200).json({ message: 'Successfully deleted todo!' })
      }
    } catch (error) {
      throw new Error(error)
    }
  }
}

module.exports = TodoController
