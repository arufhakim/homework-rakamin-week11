const express = require('express')
const router = express.Router()
const TodoController = require('../controllers/TodoController.js')

router.route('/todos').get(TodoController.getTodo).post(TodoController.createTodo)
router.route('/todos/:id').get(TodoController.getTodoById).delete(TodoController.deleteTodo)

module.exports = router
