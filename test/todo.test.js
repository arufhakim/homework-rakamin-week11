const app = require('../app.js')
const request = require('supertest')

test('Get all todos', async () => {
  try {
    const response = await request(app)
      .get('/todos')
      .expect('Content-Type', /json/)
      .expect(200)
    expect(response.body[0].id).toBe(1)
    expect(response.body[0].title).toBe('Mengerjakan Homework Rakamin')
    expect(response.body[0].deletedAt).toBe(null)
  } catch (error) {
    throw new Error(error)
  }
})

test('Get todo by id', async () => {
  try {
    const response = await request(app)
      .get('/todos/2')
      .expect('Content-Type', /json/)
      .expect(200)
    expect(response.body.id).toBe(2)
    expect(response.body.title).toBe('Mengerjakan Exam Rakamin')
    expect(response.body.deletedAt).toBe(null)
  } catch (error) {
    throw new Error(error)
  }
})

test('Create todo', async () => {
  try {
    const todo = { title: 'Monitoring Tender' }
    const response = await request(app)
      .post('/todos')
      .expect('Content-Type', /json/)
      .expect(200)
      .send(todo)
    expect(response.body.message).toBe('Successfully created todo!')
    expect(response.body.title).toBe('Monitoring Tender')
    expect(response.body.deletedAt).toBe(null)
  } catch (error) {
    throw new Error(error)
  }
})

describe('DELETE', () => {
  test('Delete todo', async () => {
    try {
      const response = await request(app).delete('/todos/5').expect(200)
      expect(response.body.message).toBe('Successfully deleted todo!')
    } catch (error) {
      throw new Error(error)
    }
  })

  test('Delete todo if Not Found', async () => {
    try {
      const response = await request(app).delete('/todos/20').expect(404)
      expect(response.body.message).toBe('Todos not found!')
    } catch (error) {
      throw new Error(error)
    }
  })
})
